import './App.css';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import { Home } from './pages/Home';
import { Login } from './pages/Login';
import { Register } from './pages/Register';
import { NotFountPage } from './pages/NotFoundPage';
import {routes} from './pages/User/routes';

export function App() {
  return (

    <div className='App'>
      <BrowserRouter>

        <nav>
          <ul>
            <li>
              <Link to={'/'}>Home Page</Link>
              <br />
              <Link to={'/Login'}>Login Page</Link>
              <br />
              <Link to={'/Register'}>Register Page</Link>
              <br />
              <Link to={'/404'}>404 Page</Link>
              <br />
              <Link to={'/User'}>User page</Link>
            </li>

          </ul>
        </nav>

        <Routes>
          <Route path='/' element={<Home />} />
          <Route path="Login" element={<Login />} />
          <Route path="Register" element={<Register />} />
          <Route path="404" element={<NotFountPage />} />

          <Route path="User">
            {routes}
          </Route>

        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
