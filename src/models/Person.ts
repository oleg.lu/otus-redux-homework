export interface Person {
    username: string;
    role: string;
}