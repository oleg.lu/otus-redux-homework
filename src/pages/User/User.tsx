import { useParams} from "react-router-dom";
import { Person } from "../../models/Person";
import { connect } from "react-redux";


interface Props {
    selecetdUser?: Person;
}
const User = (props: Props) => {
    const { selecetdUser } = props;

    const qp = useParams();

    const selectedUserStyle = {
        borderRadius: "16px",
        border: "1px solid green",
        margin: "10px",
    };
    return <div style={selectedUserStyle}>
        <label>Выбран {qp.username}</label>
        <label>{selecetdUser?.username}</label>
        <label>{selecetdUser?.role}</label>
    </div>;
}

const mapReduxStateToProps = (globalReduxState: any): Props => {
    return {
        //user.user (reducer.user)
        selecetdUser: globalReduxState.User.user,
    };
}
export default connect(mapReduxStateToProps)(User);

