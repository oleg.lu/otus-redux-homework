import { Person } from "../../models/Person";
import {useDispatch} from 'react-redux';
import { Actions } from "../../stateManagment/UserReducer";


interface Props {
    user: Person;
}

const UserItem = (props: Props) => {
    const { user } = props;
    const dispatch = useDispatch();
    const userItemStyle = {
        border: "5px solid red",
        borderRadius: "16px",
        margin: "5px",
        padding: "5px"
    }
    const select = () => {
        dispatch(Actions.setUser(user));
    };
    return <div style={userItemStyle}>
        <div>{user.username}</div>
        <div>{user.role}</div>
        <button onClick={select}>Выбрать</button>
    </div>;
}
export default UserItem