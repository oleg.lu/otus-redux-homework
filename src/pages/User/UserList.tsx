import User from "./User";
import UserItem from "./UserItem";

const users = [
    { username: 'Ivan', role: 'Admin' },
    { username: 'Andrey', role: 'CEO' },
    { username: 'Alex', role: 'Cleaner' },
];

const UserList = () => {
    return (
        <div style={{ display: "flex" }} >
            {users.map(x => <UserItem user={x} key={`oi1_${x.username}`} ></UserItem>)}
        <br/>
        <User/>
        </div>
        
    );
}

export default UserList;