import { Route } from "react-router-dom";
import UserList from "./UserList";
import User from "./User";

export default function UserRoutings() {
    return <>
        <Route index element={<UserList />} />
        <Route path=":username" element={<User />} />
        <Route path=":username/edit" element={<User />} />
    </>
}

export const routes = [
    <Route index element={<UserList />} />,
    <Route path=":username" element={<User />} />,
    <Route path=":username/edit" element={<User />} />
]