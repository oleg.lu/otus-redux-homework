import { Person } from "../models/Person";


export const Actions = Object.freeze({
    setUser: (user: Person) => ({ type: '[USER_STATE] USER_SET', payload: user })
});

interface State {
    user: Person | null;
}



const initialState: State = {
    user: null,
}

//action.type & action.payload - ключевые
const UserReducer = (state = initialState, action: any) => {
    console.log(action);
    
    if (action.type == '[USER_STATE] USER_SET')
    {
        return { ...state, user: action.payload }
    }
    return state;

};
export default UserReducer;